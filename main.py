import sys
import numpy as np
from PIL import Image

def getcol(code):
	blue =  code & 255
	green = (code >> 8) & 255
	red =   (code >> 16) & 255
	return (red, green, blue)

def mkcol(red, green, blue):
	return red * 256 * 256 + green * 256 + blue

def mkcolt(tuple):
	return tuple[0] * 256 * 256 + tuple[1] * 256 + tuple[2]

def process(FILE):
	try:
		im = Image.open(FILE)
	except:
		print("Unable to open file: " + FILE)
		print("Make sure to include picture extension, eg 'example.png', 'example.jpg'")
		exit()

	im = im.convert('RGB')

	pix = im.load()
	pc = 0
	for x in range(im.width):
		for y in range(im.height):
			pc += mkcolt(pix[x, y])

	avg = pc / (im.width * im.height * mkcol(255, 255, 255))
	thresh= round( mkcol(255, 255, 255) * avg / 1.15) 
	border = 3

	pix = im.load()
	for x in range(im.width):
		for y in range(im.height):
			pc = mkcolt(pix[x, y])
			pc = mkcol(255, 255, 255) if pc > thresh else 0

			pix[x, y] = getcol(pc)


	
	black_pix_counter = 0
	white_pix_counter = 0
	other_pix_counter = 0

	row_ratio = 0
	y_total = 0

	cutoff_thresh_hold = 0.5
	num_cutoff_pass = 0

	print("Calculating Ratio")
	for x in range(im.width):
		
		y_total = 0
		row_ratio = 0
		for y in range(im.height):
			

			black = False
			for n in range(y - border - 2, y + border):
				for m in range(x - border - 2, x + border):
					if m < 0 or m >= im.width:
						continue
					if n < 0 or n >= im.height:
						continue
					if mkcolt(pix[m, n]) == 0:
						black = True

			if not black:
				pix[x, y] = (0, 255, 255)

			y_total += 1
			row_ratio += 1
			if mkcolt(pix[x, y]) == 0:
				black_pix_counter += 1
				row_ratio -= 1
			elif pix[x, y] == (255, 255, 255):
				white_pix_counter += 1
			else:
				other_pix_counter += 1

		num_cutoff_pass += 1 if row_ratio / y_total >= cutoff_thresh_hold else -1

	print("Percent valid: ", num_cutoff_pass / im.height, num_cutoff_pass)


	im = im.convert('P')
	im.save(FILE + '_cyan.png')

	amount_all_pix = black_pix_counter + white_pix_counter + other_pix_counter
	amount_pix = black_pix_counter + white_pix_counter

	other_pix_threshhold = 0.95
	black_n_white_ratio = 0.3
	white_n_black_ratio = 0.3

	sig = True

	try:
		WA = round( white_pix_counter / amount_pix * 100)
		BA = round( black_pix_counter / amount_pix * 100)
		CR = round( other_pix_counter / amount_all_pix * 100)
		print("White ratio\t", WA, "/", round(white_n_black_ratio * 100) )
		print("Black ratio\t", BA, "/", round(black_n_white_ratio * 100) )
		print("Cyan ratio\t", CR, "/",  round(other_pix_threshhold * 100) )
		
		if other_pix_counter / amount_all_pix > other_pix_threshhold:
			sig = False

		if black_pix_counter / amount_pix < black_n_white_ratio:
			sig = False

		if round(white_pix_counter / amount_pix * 100) / 100 < white_n_black_ratio:
			sig = False
	except:
		sig = False

	if sig:
		print("Its a Signature!")
		return True
	else:
		print("Its NOT a Signature!")
		return False

	input("")


if __name__ == '__main__':
	if len(sys.argv) != 2:
		FILE = input("Enter filename of picture: ")
	else:
		FILE = sys.argv[1]
	process(FILE)



