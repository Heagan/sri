# Signature Recongnition / Identification

Made to identifies if a document is signed or not

Imagine you're in a situation where you're hiring a lot of people and need them to sign every page on your 50 page contract (why do you do this... please stop this)

Anyway you could go through each page aand check, "Yeah its signed", but that takes time.

Now image how much time this could save for corporations!
<br>
Banks for example need lawers to hand in documents signed by clients when applying for loans and need to make sure every page is signed.
<br>
My solution <b>does not use machine Learning!<\b> My method was more accurate


Runs with flask as well so you can check it out on your web browser!


# How it works

It's a python program that uses PIL library to do some image analasis and try and guess if an image is signed

The method is pretty simple:
<b>I am getting the ratio of black pixels to white pixels</b>
Theres a lot of other small checks but this is the main idea of how it works
<br>See examples below

<br>Steps:
- First I make sure I am working with an actual image and convert documents to an image if I have to
- Get image pixels and calculate the <b>mean</b>, this helps when working with dark images
- Convert it to mono-chrome using the mean as a thresh-hold
- Draw a border around every black pixel

The image left can tell us a lot and is what gives us our final answers


## Features

Works with pdf documents as well! It automatically converts pdf's to an image
<br><b>Has an API and can be called REST method</b>

## To run
```
You need python3
Install the requirments.txt

Run web version: 
python app.py or flask run

Run through terminal:
python main.py <file name>

```

# Example

![](static/uploads/curvey.jpg)

![](static/uploads/curvey.jpg_cyan.png)














