from flask import Flask, flash, request, redirect, url_for, render_template
from werkzeug import secure_filename
import os

from main import process

UPLOAD_FOLDER = 'static/uploads/'
ALLOWED_EXTENSIONS = set(['png', 'jpg', 'jpeg'])

app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.config['MAX_CONTENT_LENGTH'] = 16 * 1024 * 1024


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


@app.route('/')
@app.route('/upload')
def upload_file():
    return render_template('upload.html')


@app.route('/uploader', methods = ['GET', 'POST'])
def uploaded_file():
    render_template('temp.html')
    if request.method == 'POST':
        f = request.files['file']
        if allowed_file(f.filename):
            # render_template('upload.html', filename=full_filename)
            full_filename = os.path.join(app.config['UPLOAD_FOLDER'], f.filename)
            f.save( full_filename )
            result = "The picture is a signature" if process(full_filename) == True else "The picture is NOT a signature"
            return render_template('uploaded.html', filename=result)
    return redirect('/')


if __name__ == '__main__':
    app.run(debug=True, host="0.0.0.0", port=4444)

# @app.route('/handle_data', methods=['POST'])
# def handle_data():
# 	return "IT WORKED"
