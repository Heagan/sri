
import sys
import os
import subprocess as sub
from python_version import GoldiloxAPIClientConnection
import signal

def kill_process():

	proc = sub.Popen(['ps', '-A'], stdout=sub.PIPE, stderr=sub.PIPE)
	out, err = proc.communicate()
	os.system('lsof -i4TCP:4444 | grep LISTEN | awk \'{ print $2}\' | xargs kill')
	for line in out.splitlines():

		if 'ngrok' in str(line):
			print('killing...')
			pid = int(line.split(None, 1)[0])
			os.kill(pid, signal.SIGKILL)

def read_tracker() -> str:

	tracker_content = str()
	try:
		fd = open('.tracker', 'r')
		tracker_content = fd.read()
		fd.close()
	except FileNotFoundError as f:
		print('Error : '. f)
		sys.exit(1)
	return tracker_content


def stop_running_service():

	tracker_content = read_tracker()
	sockfd = GoldiloxAPIClientConnection('127.0.0.1', 4242)
	tracker_content = 'type^DEL&' + tracker_content + '&HTTP_URL^None'

	sockfd.Send(tracker_content)
	print('here')

        #notification = sockfd.Recv()
	kill_process()


if __name__ == '__main__':

    stop_running_service()

















