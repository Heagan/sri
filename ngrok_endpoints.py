import json
import time
import os
import subprocess as sub
import signal

from python_version import GoldiloxAPIClientConnection

def signal_handler(sig, frame):

	print('shutting down...')
	kill_process()
	exit(1)


def kill_process(): 
	
	proc = sub.Popen(['ps', '-A'], stdout=sub.PIPE, stderr=sub.PIPE)
	out, err = proc.communicate()
	os.system('lsof -i4TCP:4444 | grep LISTEN | awk \'{ print $2}\' | xargs kill')
	for line in out.splitlines():
	
		if 'ngrok' in str(line):
			print('killing...')
			pid = int(line.split(None, 1)[0])
			os.kill(pid, signal.SIGKILL)


def run():

	os.system('flask run --host=0.0.0.0 --port=4444 &')
	time.sleep(3)
	os.system('./ngrok http 4444 > /dev/null &')
	time.sleep(5)	
	os.system("curl http://localhost:4040/api/tunnels > tunnels.json")

	with open('tunnels.json') as data_file:
	    datajson = json.load(data_file)

	print('here I am')
	msg = ''
	for i in datajson['tunnels']:
		msg = msg + i['public_url'] +'\n'

	return msg


def create_tracker(ngrok_link) -> None:
	
	content = str()
	linnks = ngrok_link.split('\n')
	try:
		file = open('.tracker', 'r')
		content = file.read()
		file.close()
	except FileNotFoundError:
		print('Error : tracker file not found')
	content = content + '&HTTP_URL^' + linnks[0]
	return content


def wait_for_kill_signal(sockfd) -> None:

	content = sockfd.Recv()
	if 'kill' in content:
		kill_process()

def web_shell() -> None:

	sockfd = GoldiloxAPIClientConnection('127.0.0.1', 4242)
	ngrok_links = ''
	while True:

		ngrok_links = run()

		tracker = create_tracker(ngrok_links)
		print('trackcr is ', tracker)
		sockfd.Send(tracker)
		break 
		#wait_for_kill_signal(sockfd)


if __name__ == '__main__':

	signal.signal(signal.SIGINT, signal_handler)
	
	web_shell()




