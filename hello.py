from flask import Flask, request, render_template

app = Flask(__name__)

@app.route("/")
def hello():
    return "Hello World!"

@app.route("/path")
def path():
    return "Hello There!"

@app.route("/profile/<username>")
def profile(username):
    return render_template("profile.html", username=username)

@app.route("/method", methods=['GET', 'POST'])
def method():
	if request.method == 'POST':
		return "<h2>POST METHOD USED<h2>"
	if request.method == 'GET':
		return "<h2>GET METHOD USED<h2>"
	return "UNKNOWN METHOD USED!\n%s" % request.method

if __name__ == "__main__":
	app.run(debug=True)